from django.contrib import admin
from .models import Thing, Image, Category, Tag, Displacement


admin.site.register(Thing)
admin.site.register(Image)
admin.site.register(Category)
admin.site.register(Tag)
admin.site.register(Displacement)
