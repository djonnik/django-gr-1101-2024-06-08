from pathlib import Path
from uuid import uuid4

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.text import slugify

User = get_user_model()


def get_image_path(instance, filename: str, subfolder: Path = Path("uploads")) -> Path:
    filename = (
        f"{slugify(filename.partition(".")[0])}_{uuid4()}" + Path(filename).suffix
    )
    return Path(subfolder) / filename


class Image(models.Model):
    image = models.ImageField(upload_to=get_image_path)
    description = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.image.name


class Category(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    parent = models.ForeignKey(
        "self",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="children",
    )

    class Meta:
        verbose_name_plural = "categories"
        ordering = ["name", "parent__name"]
        unique_together = ["name", "parent"]

    def __str__(self):
        return self.name


class Tag(models.Model):
    TAG_PREFIX = "##"
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def save(
        self, force_insert=False, force_update=False, using=None, update_fields=None
    ):
        self.name = self.name.translate(str.maketrans(" ", "_", ".,;:!?#")).lower()
        self.name = self.TAG_PREFIX + self.name
        return super().save(force_insert, force_update, using, update_fields)


class Thing(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    category = models.ForeignKey(
        Category, on_delete=models.CASCADE, related_name="things"
    )
    tags = models.ManyToManyField(Tag, related_name="things")
    is_active = models.BooleanField(default=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    compensation = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name="things")
    images = models.ManyToManyField(Image, blank=True, related_name="things")

    class Meta:
        ordering = ["name", "created_at"]

    def __str__(self):
        return self.name


class Displacement(models.Model):
    class Condition(models.TextChoices):
        NEW = "N", "New - traces of use are imperceptible"
        EXCELLENT = "E", "Excellent - traces of use are minimal"
        GOOD = "G", "Good - visible signs of use"
        SATISFACTORY = "S", "Satisfactory - significant traces of use, but possible use"
        BAD = "B", "Bad - the item is damaged, but can be repaired"
        VERY_BAD = "VB", "Very bad - the item is damaged and cannot be repaired"

    thing = models.ForeignKey(
        Thing, on_delete=models.CASCADE, related_name="displacements"
    )
    to = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="displacements",
        help_text="User to whom the thing is displaced",
    )
    by = models.ForeignKey(
        User,
        on_delete=models.PROTECT,
        related_name="supported_displacements",
        help_text="User who displaced the thing",
    )
    date = models.DateTimeField(auto_now_add=True)
    is_active = models.BooleanField(default=True)
    description = models.TextField(null=True, blank=True)
    condition = models.CharField(
        max_length=2, choices=Condition, default=Condition.EXCELLENT
    )

    class Meta:
        ordering = ["-date"]

    def __str__(self):
        return f"{self.thing} -> {self.to} by {self.by}"
